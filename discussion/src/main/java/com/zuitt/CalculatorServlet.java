package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	/* 
	 * Initialization - First stage of the servlet life cycle.
	 * The "init" method is used to perform functions such as connecting to databases and initializing valus to be used in the context of the servlet.
	 * - loads the servlet.
	 * - creates an instance of the servlet class.
	 * - initialized the servlet instance by calling the init method.
	 **/
	public void init() throws ServletException{
		System.out.println("**********************");
		System.out.println("Initialized connection to database.");
		System.out.println("**********************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		int total = num1 + num2;
//		The getWriter() method is used to print out information in the browser as a response.
		PrintWriter out = res.getWriter();
		
		out.println("<h1>The total of the two numbers are " +total+ "</h1>");

	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You have accessed the get method of the calculator servlet.</h1>");
	}
	
	/*
	 * Finalization - it is the last part of the servlet life cycle that invokes the "destroy" method.
	 * - Clean up of the resources once the servlet is destroyed/unused.
	 * - Closing the connections.
	 */
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("Disconnected from database.");
		System.out.println("**********************");
	}
	
}
